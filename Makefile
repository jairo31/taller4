
CC = gcc 
INC= -I./Include/
SRC= ./Src/
OBJ= ./Obj/

cifrado: $(SRC)main.c $(OBJ)ciclico.o $(OBJ)autollave.o
	$(CC) -Wall -o ./Bin/cifrado $(SRC)main.c $(OBJ)ciclico.o $(OBJ)autollave.o $(INC) 

$(OBJ)ciclico.o: $(SRC)ciclico.c
	$(CC) -Wall -c $(SRC)ciclico.c -o $(OBJ)ciclico.o $(INC) 

$(OBJ)autollave.o: $(SRC)autollave.c
	$(CC) -Wall -c $(SRC)autollave.c -o $(OBJ)autollave.o $(INC)

clean:
	rm $(OBJ)*o ./Bin/cifrado	
