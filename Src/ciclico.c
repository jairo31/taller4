#include "cifrado.h"

void resultadoCiclico(char *texto){
	int num = 0;
	printf("Elija la llave: ");
	scanf("%d",&num);
	int i;
	char *resultado;
	resultado = malloc(sizeof(100));
	for(i=0;i<strlen(texto)-1;i++){
		if(*(texto+i)==32){
			*(resultado+i) = 32; 
		}
		else{
			*(resultado+i) = obtenerCaracterC(*(texto+i),num);		
		}
	}
	printf("EL MENSAJE CIFRADO ES: %s\n",resultado);
}


char obtenerCaracterC(char a, int i){
	if(a<90){	
		if(a+i>90){
			int e = 90 - a;
			return 65 + e;
		}
		else{	
			return a+i;
		}
	}
	else{
		if(a+i>122){
			int e = 122 - a;
			return 97 + e;
		}
		else{	
			return a+i;
		}
	}
}
